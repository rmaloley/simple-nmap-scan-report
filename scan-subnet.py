#!/usr/bin/env python3
"""
    scan-subnet.py
    author: Richard Maloley II
    email: richard [at] maloley.me
"""
#######################################
# Import required modules
#######################################

# Import sys
import sys

# Import os
import os

# Import subprocess
import subprocess

#######################################
# Global variables
#######################################
USAGE = "Usage: scan-subnet.py NETWORK/NETMASK (e.g.: scan-subnet.py 192.168.1.0/24). Must be run as superuser!"
SUBNET = ""
NETWORK = ""
NETMASK = ""

#######################################
# Custom Functions
#######################################

## A function to validate our input.
def validateInput(input):
    if len(input) > 2 or len(input) <=1:
        print("Invalid input! %s" % (input))
        return False
    else: # We have enough arguments lets see if we are valid
        if "/" not in input[1]: # Input is not valid
            print("Invalid input: %s" % (input[1]))
            return False
        else: # We have a valid input based on simple tests
            return True

#######################################
# Main Program
#######################################
if validateInput(sys.argv) == True: # We can execute our program.
    SUBNET = sys.argv[1] # Assign our subnet var
    NETWORK, NETMASK = sys.argv[1].split("/") # Assign our network/netmask vars
    XMLFILENAME = NETWORK + ".xml" # Name our file!
    HTMLFILENAME = NETWORK + ".html" # Name our file!
    subprocess.call(["nmap", "-sT", "-F", "-T4", "--exclude-ports", "T:9100", "--script", "smb-os-discovery,banner", "--stylesheet", "style.xsl", "-oX", XMLFILENAME, SUBNET]) # Subprocess to execute our command
    # Convert the XML to HTML.
    try:
        subprocess.call(["xsltproc", XMLFILENAME, "-o", HTMLFILENAME])
        print("NMAP Report HTML File: %s" % (HTMLFILENAME))
    except:
        print("Couldn't convert file.")
else: # We cannot execute our program.
    print( USAGE)
#EOF